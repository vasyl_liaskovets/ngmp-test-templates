# Node.js Global Mentoring Program - Automated Testing

This folder contains a tests designed to automatically evaluate mentee's homework tasks.

## Guidelines

When working on your part, please adhere to the following guidelines:

### 1. Organize Your Code
   - Create a dedicated folder within the `/src` directory, naming it after your module (e.g., `/src/7-nosql`).
   - Place all the tests related to your specific module within the `/tests` subfolder of your module (e.g., `/src/7-nosql/tests`).

### 2. Customization Allowed
   - You have the freedom to include any additional dependencies or ESLint rules that you deem necessary for your tasks. Feel free to enhance your workspace as needed.

### 3. Maintain Consistency
   - Ensure that your "template" remains synchronized with the provided solutions found in the `/Homework` folder. It's crucial to maintain consistency between your "template" and the solutions.
   - If the solution provided in `/Homework` is incomplete or requires updates, don't hesitate to make the necessary adjustments. The only requirement is to align it with the acceptance criteria outlined in the [book](https://d17btkcdsmqrmh.cloudfront.net/node-gmp/docs/Intro).

### 4. Clarity for Mentees
   - Feel free to modify the acceptance criteria in the book as needed to make it more understandable for mentees. Our goal is to help mentees grasp the requirements easily and achieve 100% passing tests.

If you have any questions or need assistance, please don't hesitate to reach out.

Happy coding 🚀
